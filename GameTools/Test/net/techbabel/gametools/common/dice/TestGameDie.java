package net.techbabel.gametools.common.dice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TestGameDie {
  private static final int NUM_SIDES = 20;

  private static final int NEG_SIDES = -10;

  private static final int DEFAULT_MIN = 3;

  private static final int ZERO = 0;

  private static final int ONE = 1;

  private static final String ERROR_MSG = "A die must have at least " + DEFAULT_MIN + " sides.";

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void testDie() {
    int outOfBounds = 0;

    for (int x = 1; x <= 1000; x++) {
      final Dice thisDie = new GameDice(NUM_SIDES);
      final int theRoll = thisDie.roll();

      if (theRoll < ONE || theRoll > NUM_SIDES) {
        outOfBounds++;
      }
    }

    assertEquals("Default out of bounds.", 0, outOfBounds);
  }

  @Test
  public void testNegSides() {
    thrown.expect(IndexOutOfBoundsException.class);
    thrown.expectMessage(ERROR_MSG);

    final Dice thisDie = new GameDice(NEG_SIDES);
    thisDie.roll();

    assertTrue("True", true);
  }

  @Test
  public void testZeroSides() {
    thrown.expect(IndexOutOfBoundsException.class);
    thrown.expectMessage(ERROR_MSG);

    final Dice thisDie = new GameDice(ZERO);
    thisDie.roll();

    assertTrue("True", true);
  }
}
