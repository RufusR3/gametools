package net.techbabel.gametools.common.dice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TestGameDice {
  private static final int NUM_DICE = 5;

  private static final int NUM_SIDES = 20;

  private static final int DEFAULT_MAX = NUM_DICE * NUM_SIDES;

  private static final int ZERO = 0;

  private static final int ONE = 1;

  private static final int MIN_SIDES = 3;

  private static final int NEG_DICE = -5;

  private static final int NEG_SIDES = -20;

  private static final String TRUE = "True";

  private static final String ERROR_MSG1 = "A die must have at least " + MIN_SIDES + " sides.";

  private static final String ERROR_MSG2 = "You must have at least " + ONE + " die.";

  private static final String ERROR_MSG3 = "The List total does not equal the returned total.";

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void testNumDiceNumSides() {
    int outOfBounds = 0;

    for (int x = 1; x <= 1000; x++) {
      final Dice theDice = new GameDice(NUM_DICE, NUM_SIDES);
      final int theTotal = theDice.roll();

      if (theTotal < NUM_DICE || theTotal > DEFAULT_MAX) {
        outOfBounds++;
      }
    }

    assertEquals("Default out of bounds.", 0, outOfBounds);
  }

  @Test
  public void testZeroDice() {
    thrown.expect(IndexOutOfBoundsException.class);
    thrown.expectMessage(ERROR_MSG2);

    final Dice theDice = new GameDice(ZERO, NUM_SIDES);
    theDice.roll();

    assertTrue(TRUE, true);
  }

  @Test
  public void testZeroSides() {
    thrown.expect(IndexOutOfBoundsException.class);
    thrown.expectMessage(ERROR_MSG1);

    final Dice theDice = new GameDice(NUM_DICE, ZERO);
    theDice.roll();

    assertTrue(TRUE, true);
  }

  @Test
  public void testNegDice() {
    thrown.expect(IndexOutOfBoundsException.class);
    thrown.expectMessage(ERROR_MSG2);

    final Dice theDice = new GameDice(NEG_DICE, NUM_SIDES);
    theDice.roll();

    assertTrue(TRUE, true);
  }

  @Test
  public void testNegSides() {
    thrown.expect(IndexOutOfBoundsException.class);
    thrown.expectMessage(ERROR_MSG1);

    final Dice theDice = new GameDice(NUM_DICE, NEG_SIDES);
    theDice.roll();

    assertTrue(TRUE, true);
  }

  @Test
  public void testNegDiceNegSides() {
    thrown.expect(IndexOutOfBoundsException.class);
    thrown.expectMessage(ERROR_MSG2);

    final Dice theDice = new GameDice(NEG_DICE, NEG_SIDES);
    theDice.roll();

    assertTrue(TRUE, true);
  }

  @Test
  public void testDiceList() {
    final Dice theDice = new GameDice(NUM_DICE, NUM_SIDES);
    final int theTotal = theDice.roll();
    final List<Integer> theDiceList = theDice.getDiceList();
    int listTotal = 0;

    for (final int die : theDiceList) {
      listTotal += die;
    }

    assertEquals(ERROR_MSG3, theTotal, listTotal);
  }
}
