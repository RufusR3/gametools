package net.techbabel.gametools.common.dice;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({TestGameDie.class, TestGameDice.class})
public class DiceTestSuite {

}
