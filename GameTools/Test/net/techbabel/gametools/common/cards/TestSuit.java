package net.techbabel.gametools.common.cards;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestSuit {
  private static final int NUM_SUITS = 4;

  @Test
  public void suitTest() {
    int count = 0;
    boolean foundHearts = false;
    boolean foundDiamonds = false;
    boolean foundSpades = false;
    boolean foundClubs = false;
    boolean foundAll = false;

    for (final Suit suit : Suit.values()) {
      count++;

      switch (suit) {
        case HEARTS:
          foundHearts = true;
          break;

        case DIAMONDS:
          foundDiamonds = true;
          break;

        case SPADES:
          foundSpades = true;
          break;

        case CLUBS:
          foundClubs = true;
          break;

        default:
          // Do Nothing

      }
    }

    if (foundHearts && foundDiamonds && foundSpades && foundClubs) {
      foundAll = true;
    }

    assertTrue("Could not identify all Suits.", foundAll && count == NUM_SUITS);
  }
}
