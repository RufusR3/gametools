package net.techbabel.gametools.common.cards;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.ArrayList;
import java.util.List;

import net.techbabel.gametools.common.cards.DeckOfCards.CutType;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TestDeckOfCards {
  private static final PlayingCard QUEEN_OF_HEARTS = new PlayingCard(Rank.QUEEN, Suit.HEARTS);

  private static final String FIRST_CARD = "ACE of HEARTS";

  private static final String LAST_CARD = "KING of CLUBS";

  private static final int NUMBER_HANDS = 5;

  private static final int NUMBER_CARDS = 7;

  private static final int NUMBER_REMAINING = 52 - (NUMBER_HANDS * NUMBER_CARDS);

  @Rule
  public final ExpectedException thrown = ExpectedException.none();

  @Test
  public void testDeck() {
    final DeckOfCards theDeck = new DeckOfCards();

    /***********************************************************************
     * There should be 52 cards in the deck
     **********************************************************************/
    final int size = theDeck.getNumCards();
    assertEquals("There are not 52 cards in the deck.", 52, size);
  }

  @Test
  public void testShuffle() {
    /***********************************************************************
     * Create two new decks of cards
     **********************************************************************/
    final DeckOfCards theFirstDeck = new DeckOfCards();
    final List<CardPlayer> players = new ArrayList<CardPlayer>();
    final CardPlayer cardPlayer = new CardPlayer();

    final DeckOfCards theSecondDeck = new DeckOfCards();
    final List<CardPlayer> players2 = new ArrayList<CardPlayer>();
    final CardPlayer cardPlayer2 = new CardPlayer();

    players2.add(cardPlayer2);
    theSecondDeck.deal(players2, 52);

    final List<PlayingCard> theSecondList = players2.get(0).getMyHand();

    /***********************************************************************
     * Shuffle the first deck
     **********************************************************************/
    theFirstDeck.shuffle();
    players.add(cardPlayer);
    theFirstDeck.deal(players, 52);
    final List<PlayingCard> theFirstList = players.get(0).getMyHand();

    /***********************************************************************
     * Count the number of identical cards in each deck
     **********************************************************************/
    int numSameCards = 0;
    for (int x = 0; x <= 51; x++) {
      if (theFirstList.get(x).toString().equals(theSecondList.get(x).toString())) {
        numSameCards++;
      }
    }

    /***********************************************************************
     * If the number of identical cards is 52, then they are the same
     **********************************************************************/
    assertNotSame("Shuffled deck identical to unshuffled deck.", numSameCards, 52);
  }

  @Test
  public void testRemoveTopCard() {
    final DeckOfCards theDeck = new DeckOfCards();

    /***********************************************************************
     * The top card should be FIRST_CARD
     **********************************************************************/
    final Object topCard = theDeck.removeTopCard();
    assertEquals("Remove top card wasn't the FIRST_CARD", FIRST_CARD, topCard.toString());

    /***********************************************************************
     * And there should only be 51 cards left in the deck
     **********************************************************************/
    final int size = theDeck.getNumCards();
    assertEquals("There are not 51 cards left.", 51, size);
  }

  @Test
  public void testRemoveBottomCard() {
    final DeckOfCards theDeck = new DeckOfCards();

    /***********************************************************************
     * The bottom card should be LAST_CARD
     **********************************************************************/
    final Object bottomCard = theDeck.removeBottomCard();
    assertEquals("Remove top card wasn't the LAST_CARD", LAST_CARD, bottomCard.toString());

    /***********************************************************************
     * And there should only be 51 cards left in the deck
     **********************************************************************/
    final int size = theDeck.getNumCards();
    assertEquals("There are not 51 cards left.", 51, size);
  }

  @Test
  public void testRandomCard() {
    final DeckOfCards theDeck = new DeckOfCards();

    /***********************************************************************
     * There should only be 51 cards left in the deck
     **********************************************************************/
    final PlayingCard randomCard = theDeck.removeRandomCard();
    final int size = theDeck.getNumCards();
    final String theCard = randomCard.toString();

    assertEquals("There are not 51 cards left. [" + theCard + "]", 51, size);
  }

  @Test
  public void testDealPlayer() {
    final DeckOfCards theDeck = new DeckOfCards();
    theDeck.shuffle();

    final List<CardPlayer> playerList = new ArrayList<CardPlayer>();

    /***********************************************************************
     * Create NUMBER_HANDS Players
     **********************************************************************/
    for (int x = 1; x <= NUMBER_HANDS; x++) {
      final CardPlayer player = new CardPlayer();
      playerList.add(player);
    }

    /***********************************************************************
     * Deal NUMBER_CARDS to each Player
     **********************************************************************/
    theDeck.deal(playerList, NUMBER_CARDS);

    /***********************************************************************
     * There should only be 52 - (NUMBER_HANDS * NUM_CARDS) cards left
     **********************************************************************/
    final int size = theDeck.getNumCards();
    assertEquals("There are more than NUMBER_REMAINING cards after the deal.", NUMBER_REMAINING,
        size);

    /***********************************************************************
     * Each player should have NUMBER_CARDS
     **********************************************************************/
    for (final CardPlayer player : playerList) {
      final List<PlayingCard> hand = player.getMyHand();
      assertEquals("The Player did not have NUMBER_CARDS in the hand.", NUMBER_CARDS, hand.size());
    }
  }

  @Test
  public void testDealingCards() {
    final DeckOfCards theDeck = new DeckOfCards();

    theDeck.shuffle();

    /***********************************************************************
     * Deal NUMBER_HANDS hands NUMBER_CARDS cards
     **********************************************************************/
    final List<CardPlayer> players = new ArrayList<CardPlayer>();

    for (int x = 1; x <= NUMBER_HANDS; x++) {
      final CardPlayer player = new CardPlayer();
      players.add(player);
    }

    theDeck.deal(players, NUMBER_CARDS);

    /***********************************************************************
     * There should be NUMBER_HANDS dealt
     **********************************************************************/
    assertEquals("There are not NUMBER_HANDS dealt.", NUMBER_HANDS, players.size());

    /***********************************************************************
     * Each hand should have NUMBER_CARDS cards
     **********************************************************************/
    for (final CardPlayer player : players) {
      assertEquals("There are not NUMBER_CARDS in each hand.", NUMBER_CARDS, player.getMyHand()
          .size());
    }

    /***********************************************************************
     * There should only be 52 - (NUMBER_HANDS * NUM_CARDS) cards left
     **********************************************************************/
    final int size = theDeck.getNumCards();
    assertEquals("There are more than NUMBER_REMAINING cards after the deal.", NUMBER_REMAINING,
        size);
  }

  @Test
  public void testCuttingCardsMed() {
    final DeckOfCards theDeck = new DeckOfCards();

    theDeck.cut(CutType.MED);

    /***********************************************************************
     * The top card should not be FIRST_CARD
     **********************************************************************/
    final Object topCard = theDeck.removeTopCard();
    assertNotSame("The top card should not be FIRST_CARD", FIRST_CARD, topCard.toString());

    /***********************************************************************
     * And the bottom card should not be LAST_CARD
     **********************************************************************/
    final Object bottomCard = theDeck.removeBottomCard();
    assertNotSame("Remove top card wasn't the LAST_CARD", LAST_CARD, bottomCard.toString());

    /***********************************************************************
     * And there should only be 50 cards left in the deck
     **********************************************************************/
    final int size = theDeck.getNumCards();
    assertEquals("There are not 51 cards left.", 50, size);
  }

  @Test
  public void testCuttingCardsHigh() {
    final DeckOfCards theDeck = new DeckOfCards();

    theDeck.cut(CutType.HIGH);

    /***********************************************************************
     * The top card should not be FIRST_CARD
     **********************************************************************/
    final Object topCard = theDeck.removeTopCard();
    assertNotSame("The top card should not be FIRST_CARD", FIRST_CARD, topCard.toString());

    /***********************************************************************
     * And the bottom card should not be LAST_CARD
     **********************************************************************/
    final Object bottomCard = theDeck.removeBottomCard();
    assertNotSame("Remove top card wasn't the LAST_CARD", LAST_CARD, bottomCard.toString());

    /***********************************************************************
     * And there should only be 50 cards left in the deck
     **********************************************************************/
    final int size = theDeck.getNumCards();
    assertEquals("There are not 51 cards left.", 50, size);
  }

  @Test
  public void testCuttingCardsLow() {
    final DeckOfCards theDeck = new DeckOfCards();

    theDeck.cut(CutType.LOW);

    /***********************************************************************
     * The top card should not be FIRST_CARD
     **********************************************************************/
    final Object topCard = theDeck.removeTopCard();
    assertNotSame("The top card should not be FIRST_CARD", FIRST_CARD, topCard.toString());

    /***********************************************************************
     * And the bottom card should not be LAST_CARD
     **********************************************************************/
    final Object bottomCard = theDeck.removeBottomCard();
    assertNotSame("Remove top card wasn't the LAST_CARD", LAST_CARD, bottomCard.toString());

    /***********************************************************************
     * And there should only be 50 cards left in the deck
     **********************************************************************/
    final int size = theDeck.getNumCards();
    assertEquals("There are not 51 cards left.", 50, size);
  }

  @Test
  public void testRemoveCard() {
    final DeckOfCards theDeck = new DeckOfCards();
    theDeck.shuffle();

    theDeck.removeCard(QUEEN_OF_HEARTS);

    /***********************************************************************
     * There should only be 51 cards left in the deck
     **********************************************************************/
    final int size = theDeck.getNumCards();
    assertEquals("There are not 51 cards left after removing the " + QUEEN_OF_HEARTS.toString()
        + ".", 51, size);

    /***********************************************************************
     * And the QUEEN_OF_HEARTS should not be left in the deck
     **********************************************************************/
    final CardPlayer cardPlayer = new CardPlayer();
    final List<CardPlayer> players = new ArrayList<CardPlayer>();

    players.add(cardPlayer);
    theDeck.deal(players, size);
    final List<PlayingCard> theHand = players.get(0).getMyHand();

    for (final PlayingCard card : theHand) {
      assertNotSame(QUEEN_OF_HEARTS.toString() + " is still in the deck.", card, QUEEN_OF_HEARTS);
    }
  }

  @Test
  public void discardDealResuffleTest() {
    final DeckOfCards theDeck = new DeckOfCards();
    final List<PlayingCard> discardPile = new ArrayList<PlayingCard>();
    final List<CardPlayer> players = new ArrayList<CardPlayer>();

    /***********************************************************************
     * Create five new players
     **********************************************************************/
    for (int x = 1; x <= 5; x++) {
      players.add(new CardPlayer());
    }

    /***********************************************************************
     * Deal 5 cards to each player
     **********************************************************************/
    theDeck.deal(players, 5);

    /***********************************************************************
     * Have each player discard 2 cards
     **********************************************************************/
    for (int x = 1; x <= 2; x++) {
      for (CardPlayer player : players) {
        discardPile.add(player.discard(player.getMyHand().get(0)));
      }
    }

    theDeck.reShuffle(discardPile);

    /***********************************************************************
     * The new deck of cards should have 37 cards
     **********************************************************************/
    assertEquals("The deck was not reshuffled.", theDeck.getNumCards(), 37);

    /***********************************************************************
     * And the discard pile should be empty
     **********************************************************************/
    assertEquals("The discard pile still has cards after a reshuffe!", 0, discardPile.size());
  }

  @Test
  public void discardReshuffleTest() {
    thrown.expect(DuplicateCardsIdentifiedException.class);

    final PlayingCard aceOfSpades = new PlayingCard(Rank.ACE, Suit.SPADES);
    final PlayingCard sevenOfHearts = new PlayingCard(Rank.SEVEN, Suit.HEARTS);
    final PlayingCard threeOfDiamonds = new PlayingCard(Rank.THREE, Suit.DIAMONDS);
    final PlayingCard sixOfClubs = new PlayingCard(Rank.SIX, Suit.CLUBS);

    final DeckOfCards theDeck = new DeckOfCards();

    theDeck.shuffle();
    theDeck.cut(CutType.MED);

    final List<PlayingCard> discardPile = new ArrayList<PlayingCard>();
    discardPile.add(aceOfSpades);
    discardPile.add(sevenOfHearts);
    discardPile.add(threeOfDiamonds);
    discardPile.add(sixOfClubs);

    theDeck.reShuffle(discardPile);

    /***********************************************************************
     * There are duplicates, so a reshuffle should not happen
     **********************************************************************/
    assertEquals("Reshuffled duplicate cards into the deck", theDeck.getNumCards(),
        theDeck.getFullDeckSize());
  }
}
