package net.techbabel.gametools.common.cards;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TestCardPlayer {
  private static DeckOfCards theDeck;

  private static int deckSize;

  private static List<PlayingCard> testHand;

  private static final int DRAW_COUNT = 5;

  @Before
  public void setUp() {
    final PlayingCard ACE_OF_HEARTS = new PlayingCard(Rank.ACE, Suit.HEARTS);
    final PlayingCard TWO_OF_HEARTS = new PlayingCard(Rank.TWO, Suit.HEARTS);
    final PlayingCard THREE_OF_HEARTS = new PlayingCard(Rank.THREE, Suit.HEARTS);
    final PlayingCard FOUR_OF_HEARTS = new PlayingCard(Rank.FOUR, Suit.HEARTS);
    final PlayingCard FIVE_OF_HEARTS = new PlayingCard(Rank.FIVE, Suit.HEARTS);

    testHand = new ArrayList<PlayingCard>();
    testHand.add(ACE_OF_HEARTS);
    testHand.add(TWO_OF_HEARTS);
    testHand.add(THREE_OF_HEARTS);
    testHand.add(FOUR_OF_HEARTS);
    testHand.add(FIVE_OF_HEARTS);

    theDeck = new DeckOfCards();
    deckSize = theDeck.getNumCards() - DRAW_COUNT;
  }

  @Test
  public void testDraw() {
    final CardPlayer thePlayer = new CardPlayer();

    for (int x = 1; x <= DRAW_COUNT; x++) {
      thePlayer.draw(theDeck);
    }

    final int handSize = thePlayer.getMyHand().size();

    // Make sure the Player's hand has the correct number of cards
    assertEquals("The player's hand did not have " + DRAW_COUNT + " card(s).", handSize, DRAW_COUNT);

    // The deck should also have the correct number of cards
    assertEquals("The size of the deck is not correct.", theDeck.getNumCards(), deckSize);

    // The hand should be the same as the test hand
    assertEquals("The hand is not what was expected.", thePlayer.getMyHand(), testHand);
  }

  @Test
  public void testDiscard() {
    final CardPlayer thePlayer = new CardPlayer();

    for (int x = 1; x <= DRAW_COUNT; x++) {
      thePlayer.draw(theDeck);
    }

    final PlayingCard discardCard = thePlayer.discard(new PlayingCard(Rank.THREE, Suit.HEARTS));

    final int handSize = thePlayer.getMyHand().size();

    // Make sure the Player's hand has the correct number of cards
    assertEquals("The player's hand did not have " + (DRAW_COUNT - 1) + " card(s).", handSize,
        DRAW_COUNT - 1);

    // The discarded card should be the Three of Hearts
    assertEquals("The wrong card was discarded.", discardCard, new PlayingCard(Rank.THREE,
        Suit.HEARTS));
  }
}
