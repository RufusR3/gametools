package net.techbabel.gametools.common.cards;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({TestRank.class, TestSuit.class, TestPlayingCard.class, TestDeckOfCards.class,
    TestCardPlayer.class})
public class CardsTestSuite {

}
