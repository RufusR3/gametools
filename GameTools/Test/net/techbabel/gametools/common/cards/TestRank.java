package net.techbabel.gametools.common.cards;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestRank {
  private static final int NUM_RANKS = 13;

  @Test
  public void rankTest() {
    int count = 0;
    boolean foundAce = false;
    boolean foundTwo = false;
    boolean foundThree = false;
    boolean foundFour = false;
    boolean foundFive = false;
    boolean foundSix = false;
    boolean foundSeven = false;
    boolean foundEight = false;
    boolean foundNine = false;
    boolean foundTen = false;
    boolean foundJack = false;
    boolean foundQueen = false;
    boolean foundKing = false;
    boolean foundAll = false;

    for (final Rank rank : Rank.values()) {
      count++;

      switch (rank) {
        case ACE:
          foundAce = true;
          break;

        case TWO:
          foundTwo = true;
          break;

        case THREE:
          foundThree = true;
          break;

        case FOUR:
          foundFour = true;
          break;

        case FIVE:
          foundFive = true;
          break;

        case SIX:
          foundSix = true;
          break;

        case SEVEN:
          foundSeven = true;
          break;

        case EIGHT:
          foundEight = true;
          break;

        case NINE:
          foundNine = true;
          break;

        case TEN:
          foundTen = true;
          break;

        case JACK:
          foundJack = true;
          break;

        case QUEEN:
          foundQueen = true;
          break;

        case KING:
          foundKing = true;
          break;

        default:
          // Do nothing
      }
    }

    if (foundAce && foundTwo && foundThree && foundFour && foundFive && foundSix && foundSeven
        && foundEight && foundNine && foundTen && foundJack && foundQueen && foundKing) {
      foundAll = true;
    }

    assertTrue("Could not identify all Ranks.", foundAll && count == NUM_RANKS);
  }
}
