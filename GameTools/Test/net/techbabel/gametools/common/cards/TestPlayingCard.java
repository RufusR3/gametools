package net.techbabel.gametools.common.cards;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TestPlayingCard {
  private static final String ACE_OF_SPADES = "ACE of SPADES";

  private static final PlayingCard SEVEN_OF_CLUBS = new PlayingCard(Rank.SEVEN, Suit.CLUBS);

  private static final int TOTAL_CARDS = 52;

  @Test
  public void playingCardTest() {
    final PlayingCard theCard = new PlayingCard(Rank.ACE, Suit.SPADES);

    assertEquals("Could not create the Ace of Spades", ACE_OF_SPADES, theCard.toString());
  }

  @Test
  public void allCardsTest() {
    String theCard;
    PlayingCard playingCard;

    for (final Suit suit : Suit.values()) {
      for (final Rank rank : Rank.values()) {
        playingCard = new PlayingCard(rank, suit);
        theCard = playingCard.toString();

        final StringBuilder thisCard = new StringBuilder();
        thisCard.append(rank).append(" of ").append(suit);

        assertEquals("Incorrect playing card created.", thisCard.toString(), theCard);
      }
    }
  }

  @Test
  public void countCardsTest() {
    final List<PlayingCard> theCards = new ArrayList<PlayingCard>();

    for (final Suit suit : Suit.values()) {
      for (final Rank rank : Rank.values()) {
        theCards.add(new PlayingCard(rank, suit));
      }
    }

    assertEquals("Could not create all " + TOTAL_CARDS + " cards.", TOTAL_CARDS, theCards.size());
  }

  @Test
  public void cardsEqualTest() {
    final PlayingCard sevenOfClubs = new PlayingCard(Rank.SEVEN, Suit.CLUBS);

    assertEquals(
        "The " + sevenOfClubs.toString() + " is not the same as the " + SEVEN_OF_CLUBS.toString()
            + ".", sevenOfClubs, SEVEN_OF_CLUBS);
  }

  @Test
  public void cardsNotEqualTest() {
    final PlayingCard queenOfHearts = new PlayingCard(Rank.QUEEN, Suit.HEARTS);

    assertNotSame(
        "The " + queenOfHearts.toString() + " is the same as the " + SEVEN_OF_CLUBS.toString()
            + ".", queenOfHearts, SEVEN_OF_CLUBS);
  }
}
