package net.techbabel.gametools.common.random;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({RandomNumberTest.class})
public class RandomTestSuite {

}
