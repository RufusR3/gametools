package net.techbabel.gametools.common.random;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class RandomNumberTest {
  private static final int RANDOM_MIN = 1;

  private static final int RANDOM_MAX = 400;

  private static final int NEG_FIFTY = -50;

  private static final int NEG_ONE_HUNDRED = -100;

  private static final int NEG_TWO_HUNDRED = -200;

  private static final int FIVE = 5;

  private static final int TEN = 10;

  private static final int ONE_HUNDRED = 100;

  private static final int TWO_HUNDRED = 200;

  private static final String ERROR_MSG = "Provided MAX less than provided MIN.";

  @Rule
  public final ExpectedException thrown = ExpectedException.none();

  @Test
  public void testGenerateMax() {
    int outOfBounds = 0;
    for (int x = 1; x <= 1000; x++) {
      final int theNumber = RandomNumber.generate(RANDOM_MAX);
      if (theNumber < RANDOM_MIN || theNumber > RANDOM_MAX) {
        outOfBounds++;
      }
    }

    assertEquals("Generated number out of bounds (MAX).", 0, outOfBounds);
  }

  @Test
  public void testGenerateNegMax() {
    thrown.expect(IndexOutOfBoundsException.class);
    thrown.expectMessage(ERROR_MSG);

    final Integer theNumber = RandomNumber.generate(NEG_FIFTY);

    assertEquals("Didn't return NULL. (MAX)", null, theNumber);
  }

  @Test
  public void testGenerateMinMax() {
    int outOfBounds = 0;

    for (int x = 1; x <= 1000; x++) {
      final int theNumber = RandomNumber.generate(ONE_HUNDRED, TWO_HUNDRED);
      if (theNumber < ONE_HUNDRED || theNumber > TWO_HUNDRED) {
        outOfBounds++;
      }
    }

    assertEquals("Generated number out of bounds (MIN, MAX).", 0, outOfBounds);
  }

  @Test
  public void testGenerateNegMinMax() {
    int outOfBounds = 0;

    for (int x = 1; x <= 1000; x++) {
      final int theNumber = RandomNumber.generate(NEG_ONE_HUNDRED, ONE_HUNDRED);
      if (theNumber < NEG_ONE_HUNDRED || theNumber > ONE_HUNDRED) {
        outOfBounds++;
      }
    }

    assertEquals("Generated number out of bounds (NEGMIN, MAX).", 0, outOfBounds);
  }

  @Test
  public void testGenerateNegMinNegMax() {
    int outOfBounds = 0;

    for (int x = 1; x <= 1000; x++) {
      final int theNumber = RandomNumber.generate(NEG_TWO_HUNDRED, NEG_ONE_HUNDRED);
      if (theNumber < NEG_TWO_HUNDRED || theNumber > NEG_ONE_HUNDRED) {
        outOfBounds++;
      }
    }

    assertEquals("Generated number out of bounds (NEGMIN, NEGMAX).", 0, outOfBounds);
  }

  @Test
  public void testGenerateMinBadMax() {
    thrown.expect(IndexOutOfBoundsException.class);
    thrown.expectMessage(ERROR_MSG);

    final Integer theNumber = RandomNumber.generate(TEN, FIVE);

    assertEquals("Didn't return NULL. (MIN BADMAX)", null, theNumber);
  }
}
