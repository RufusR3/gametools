import net.techbabel.gametools.common.cards.CardsTestSuite;
import net.techbabel.gametools.common.dice.DiceTestSuite;
import net.techbabel.gametools.common.random.RandomTestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({RandomTestSuite.class, DiceTestSuite.class, CardsTestSuite.class})
public final class FullTestSuite {

}
