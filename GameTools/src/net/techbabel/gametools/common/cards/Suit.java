package net.techbabel.gametools.common.cards;

/**
 * Enumeration of the Suits a playing card can have.
 * 
 * @author Rufus
 */
public enum Suit {
  HEARTS, DIAMONDS, SPADES, CLUBS
}
