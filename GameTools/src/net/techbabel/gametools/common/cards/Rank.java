package net.techbabel.gametools.common.cards;

/**
 * Enumeration of the ranks a playing card can have.
 * 
 * @author Rufus
 */
public enum Rank {
  ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING
}
