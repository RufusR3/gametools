package net.techbabel.gametools.common.cards;

/**
 * Thrown to indicate that duplicate cards have been identified in a situation where duplicate cards
 * are not permitted.
 * 
 * @author Rufus
 */
public class DuplicateCardsIdentifiedException extends RuntimeException {

  private static final long serialVersionUID = 855894461266938403L;

  /**
   * Constructs a <code>DuplicateCardsIdentifiedException</code> with no detailed message.
   */
  public DuplicateCardsIdentifiedException() {
    super();
  }

  /**
   * Constructs a <code>DuplicateCardsIdentifiedException</code> with the specified detailed
   * message.
   * 
   * @param s - The detailed message.
   */
  public DuplicateCardsIdentifiedException(final String s) {
    super(s);
  }
}
