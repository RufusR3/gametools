package net.techbabel.gametools.common.cards;

/**
 * Class to represent a real world playing card.
 * 
 * @author rreynolds
 */
public class PlayingCard {
  // Define instance variables
  private final Suit theSuit;

  private final Rank theRank;

  /**
   * Create a new Card with the given suit and rank.
   * 
   * @param suit - The suit to make the card.
   * @param rank - The rank to make the card.
   */
  public PlayingCard(final Rank rank, final Suit suit) {
    this.theRank = rank;
    this.theSuit = suit;
  }

  /**
   * Override java.lang.Object.toString to show the Rank and Suit of the card.
   */
  @Override
  public String toString() {
    return this.theRank + " of " + this.theSuit;
  }

  /**
   * Override java.lang.Object.hashCode
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;

    result = prime * result + ((theRank == null) ? 0 : theRank.hashCode());
    result = prime * result + ((theSuit == null) ? 0 : theSuit.hashCode());

    return result;
  }

  /**
   * Override java.lang.Object.equals to compare PlayingCards
   */
  @Override
  public boolean equals(final Object obj) {
    boolean isEqual = true;

    if (obj == null || !(obj instanceof PlayingCard)) {
      isEqual = false;
    } else {
      final PlayingCard other = (PlayingCard) obj;

      if (theRank != other.theRank || theSuit != other.theSuit) {
        isEqual = false;
      }
    }

    return isEqual;
  }
}
