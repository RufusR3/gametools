package net.techbabel.gametools.common.cards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.techbabel.gametools.common.random.RandomNumber;

/**
 * Class to emulate a real world deck of cards.
 * 
 * @author Rufus
 */
public final class DeckOfCards {
  // Define instance variables
  private final List<PlayingCard> theDeck;

  private final int fullDeckSize;

  // Define Constants
  private static final int EMPTY_DECK = 0;

  // Upper and lower bounds for number of times to shuffle
  private static final int MIN_NUM_SHUFFLES = 7;
  private static final int MAX_NUM_SHUFFLES = 12;

  // Minimum size a deck can be for shuffling
  private static final int MIN_SHUFFLE_SIZE = 2;

  // Portion of deck to use for cutting
  private static final float LOW_CUT = 3F / 4;
  private static final float MID_CUT = 1F / 2;
  private static final float HIGH_CUT = 1F / 4;

  // Enumerations for cutting the deck
  public enum CutType {
    LOW, MED, HIGH
  }

  // Initial cut pad
  private static final int CUT_PAD = 5;

  /**
   * Create a new deck of cards with all 52 unique cards.
   */
  public DeckOfCards() {
    final List<PlayingCard> thisDeck = new ArrayList<PlayingCard>();

    // Create a new card for each Suit and each Rank
    for (final Suit suit : Suit.values()) {
      for (final Rank rank : Rank.values()) {
        thisDeck.add(new PlayingCard(rank, suit));
      }
    }

    this.theDeck = new ArrayList<PlayingCard>();
    this.theDeck.addAll(thisDeck);
    this.fullDeckSize = this.theDeck.size();
  }

  /**
   * Return the number of cards in the current deck.
   * 
   * @return The number of cards in the current deck.
   */
  public int getNumCards() {
    return this.theDeck.size();
  }

  /**
   * Randomize the order of the cards within the deck.
   */
  public void shuffle() {
    final int deckSize = this.getNumCards();
    final int numShuffles = RandomNumber.generate(MIN_NUM_SHUFFLES, MAX_NUM_SHUFFLES);

    // If we have enough cards in the deck, perform the shuffle
    if (deckSize >= MIN_SHUFFLE_SIZE) {
      for (int i = 1; i <= numShuffles; i++) {
        Collections.shuffle(this.theDeck, RandomNumber.getRandomGenerator());
      }
    }
  }

  /**
   * Cut the current deck.
   * 
   * @param cutType - Indicates to cut high, middle, or low on the deck.
   */
  public void cut(final CutType cutType) {
    float cutPoint;

    switch (cutType) {
      case LOW:
        cutPoint = LOW_CUT;
        break;

      case MED:
        cutPoint = MID_CUT;
        break;

      case HIGH:
        cutPoint = HIGH_CUT;
        break;

      default:
        cutPoint = MID_CUT;
        break;
    }

    // Determine where to cut the deck
    final int deckSize = this.getNumCards();
    int cutPad = CUT_PAD;
    final float cutSize = deckSize * cutPoint - 1;

    do {
      // Make sure we can cut the deck with the current padding,
      // otherwise decrease the pad.
      if (cutSize - cutPad > 1 && cutSize + cutPad < deckSize - 1) {
        final int rndPad = RandomNumber.generate(-cutPad, cutPad);
        final int cardLocation = (int) (deckSize * cutPoint + rndPad);
        final List<PlayingCard> halfDeck = new ArrayList<PlayingCard>();

        // Place the top cut into a temporary list
        halfDeck.addAll(this.theDeck.subList(0, cardLocation));

        // Remove the top cut from the deck and place it on the bottom
        this.theDeck.subList(0, cardLocation).clear();
        this.theDeck.addAll(halfDeck);
        break;
      } else {
        cutPad--;
      }
      // Until there aren't enough cards to cut.
    } while (deckSize / 2 - cutPad >= 0 && deckSize / 2 + cutPad <= deckSize);
  }

  /**
   * Remove a specific card from the current deck.
   * 
   * @param theCard - the specific card to remove from the deck.
   * @return the specific card removed.
   */
  public PlayingCard removeCard(final PlayingCard theCard) {
    PlayingCard removedCard = null;
    final int cardIndex = this.theDeck.indexOf(theCard);

    // Only remove the card if it exists in the deck
    if (cardIndex != -1) {
      removedCard = this.theDeck.get(cardIndex);
      this.theDeck.remove(cardIndex);
    }

    return removedCard;
  }

  /**
   * Remove the top card from the current deck.
   * 
   * @return The card from the top of the deck.
   */
  public PlayingCard removeTopCard() {
    PlayingCard topCard = null;
    final int deckSize = this.getNumCards();

    // If we have cards, remove the top one
    if (deckSize > EMPTY_DECK) {
      topCard = this.theDeck.get(0);
      this.theDeck.remove(0);
    }

    return topCard;
  }

  /**
   * Remove the bottom card from the current deck.
   * 
   * @return The card from the bottom of the deck.
   */
  public PlayingCard removeBottomCard() {
    PlayingCard bottomCard = null;
    final int deckSize = this.getNumCards();

    // If we have the cards, remove the bottom one
    if (deckSize > EMPTY_DECK) {
      bottomCard = this.theDeck.get(deckSize - 1);
      this.theDeck.remove(deckSize - 1);
    }

    return bottomCard;
  }

  /**
   * Remove a random card from the current deck.
   * 
   * @return The card from a random position within the deck.
   */
  public PlayingCard removeRandomCard() {
    PlayingCard randomCard = null;
    final int deckSize = this.getNumCards();

    // If we have the cards, remove a random one
    if (deckSize > EMPTY_DECK) {
      final int theNumber = RandomNumber.generate(0, deckSize - 1);
      randomCard = this.theDeck.get(theNumber);
      this.theDeck.remove(theNumber);
    }

    return randomCard;
  }

  /**
   * Method to deal the cards to a List of CardPlayers.
   * 
   * @param players - The Players that will be dealt to.
   * @param numCards - The number of cards to deal each player.
   */
  public void deal(final List<CardPlayer> players, final int numCards) {
    for (int x = 1; x <= numCards; x++) {
      for (final CardPlayer player : players) {
        player.draw(this);
      }
    }
  }

  /**
   * Method to take the remaining cards from the deck and shuffle it together with the cards from
   * the discard pile to create a new deck of cards and an empty discard pile.
   */
  public void reShuffle(final List<PlayingCard> discardPile) {
    boolean foundDups = false;
    final List<PlayingCard> identifiedDups = new ArrayList<PlayingCard>();

    // Make sure there aren't any duplicates
    for (final PlayingCard card : discardPile) {
      if (this.theDeck.contains(card)) {
        foundDups = true;
        identifiedDups.add(card);
      }
    }

    if (foundDups) {
      final StringBuilder errorMessage =
          new StringBuilder(
              "The following cards in the discard pile are duplicates of those in the remainder of the deck. The reshuffle failed. ")
              .append(identifiedDups.toString());

      throw new DuplicateCardsIdentifiedException(errorMessage.toString());
    } else {
      Collections.shuffle(discardPile, RandomNumber.getRandomGenerator());

      this.theDeck.addAll(discardPile);
      this.shuffle();
      discardPile.clear();
    }
  }

  /**
   * Getter for the full deck size.
   * 
   * @return The size of a full deck.
   */
  public int getFullDeckSize() {
    return this.fullDeckSize;
  }
}
