package net.techbabel.gametools.common.cards;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to emulate a card player.
 * 
 * @author rreynolds
 * 
 */
public class CardPlayer {
  // Define instance variables
  private final List<PlayingCard> myHand;

  /**
   * Create a new player and initialize the hand.
   */
  public CardPlayer() {
    this.myHand = new ArrayList<PlayingCard>();
  }

  /**
   * Method to draw a card from the top of the deck.
   * 
   * @param theDeck - The deck of cards to draw from.
   */
  public void draw(final DeckOfCards theDeck) {
    // Remove the top card and add it to the hand
    final PlayingCard drawCard = theDeck.removeTopCard();
    this.myHand.add(drawCard);
  }

  public void discard() {
    // Not sure if we need a discard with no parameters
  }

  /**
   * Method to discard the given card.
   * 
   * @param thisCard - The card to discard.
   * @return The discarded card.
   */
  public PlayingCard discard(final PlayingCard thisCard) {
    PlayingCard removedCard = null;

    final int cardIndex = this.myHand.indexOf(thisCard);

    // Only remove the card if it exists in the deck
    if (cardIndex != -1) {
      removedCard = this.myHand.get(cardIndex);
      this.myHand.remove(cardIndex);
    }

    return removedCard;
  }

  /**
   * Method to retrieve the Player's hand.
   * 
   * @return The hand of cards.
   */
  public List<PlayingCard> getMyHand() {
    final List<PlayingCard> thisHand = new ArrayList<PlayingCard>();
    thisHand.addAll(this.myHand);

    return thisHand;
  }
}
