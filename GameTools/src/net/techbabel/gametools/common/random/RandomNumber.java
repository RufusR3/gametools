package net.techbabel.gametools.common.random;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Static Singleton implementation of a Random Number Generator. The Generator will be re-seeded at
 * random intervals of use.
 * 
 * @author Rufus
 */
public enum RandomNumber {
  INSTANCE;

  // Define instance variables
  private static Random nonSecureGenerator;

  private static SecureRandom secureGenerator;

  private static boolean isSecure;

  // Keep track of how often a number has been generated so we can re-seed as
  // needed
  private static int numTimesUsed;

  // Define the max number of times used before we re-seed
  private static int useThreshold = 50;

  // Set the MIN and MAX to get a random threshold for re-seeding
  private static final int THRESHOLD_MIN = 100;

  private static final int THRESHOLD_MAX = 1000;

  // Define the Array Size for the SecureRandom object
  private static final int ARRAY_SIZE = 16;

  private static final String ERROR_MSG = "Provided MAX less than provided MIN.";

  // Define the Algorithm and Provider to create the SecureRandom object
  private static final String RNG_ALGORITHM = "SHA1PRNG";

  private static final String RNG_PROVIDER = "SUN";

  /**
   * Use a secure random object if we can.
   */
  static {
    try {
      secureGenerator = SecureRandom.getInstance(RNG_ALGORITHM, RNG_PROVIDER);
      secureGenerator.nextBytes(new byte[ARRAY_SIZE]);
      isSecure = true;
    }

    catch (NoSuchAlgorithmException | NoSuchProviderException e) {
      nonSecureGenerator = new Random(System.currentTimeMillis());
    }

    // Set the number of times before a re-seed is needed to be a random
    // value
    useThreshold = RandomNumber.generate(THRESHOLD_MIN, THRESHOLD_MAX);
  }

  /**
   * Generate a random number from min to max (inclusive).
   * 
   * @param min - The minimum value the random number can be.
   * @param max - The maximum value the random number can be.
   * @return The random number.
   * @throws IndexOutOfBoundsException when the maximum provided is less than the minimum.
   */
  public static final Integer generate(final int min, final int max)
      throws IndexOutOfBoundsException {
    int randomNumber = 0;

    if (max >= min) {
      // If we are at the threshold, re-generate the seed
      if (numTimesUsed >= useThreshold) {
        numTimesUsed = 0;
        useThreshold = RandomNumber.generate(THRESHOLD_MIN, THRESHOLD_MAX);

        if (isSecure) {
          secureGenerator.setSeed(secureGenerator.generateSeed(ARRAY_SIZE));
        } else {
          nonSecureGenerator = new Random(System.currentTimeMillis());
        }
      }

      if (isSecure) {
        randomNumber = secureGenerator.nextInt(max - min + 1) + min;
      } else {
        randomNumber = nonSecureGenerator.nextInt(max - min + 1) + min;
      }

    } else {
      throw new IndexOutOfBoundsException(ERROR_MSG);
    }

    numTimesUsed++;
    return randomNumber;
  }

  /**
   * Generate a random number from 1 to max (inclusive).
   * 
   * @param max - The maximum value the random number can be.
   * @return The random number.
   */
  public static final Integer generate(final int max) {
    return generate(1, max);
  }

  /**
   * Get the current Generator.
   * 
   * @return The Generator used to create random numbers.
   */
  public static final Random getRandomGenerator() {
    Random thisGenerator = null;

    if (numTimesUsed >= useThreshold) {
      numTimesUsed = 0;
      useThreshold = RandomNumber.generate(THRESHOLD_MIN, THRESHOLD_MAX);

      if (isSecure) {
        secureGenerator.setSeed(secureGenerator.generateSeed(ARRAY_SIZE));
      } else {
        nonSecureGenerator = new Random(System.currentTimeMillis());
      }
    }

    if (isSecure) {
      thisGenerator = secureGenerator;
    } else {
      thisGenerator = nonSecureGenerator;
    }

    numTimesUsed++;
    return thisGenerator;
  }
}
