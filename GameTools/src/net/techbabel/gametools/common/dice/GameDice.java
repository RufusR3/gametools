package net.techbabel.gametools.common.dice;

import java.util.ArrayList;
import java.util.List;

import net.techbabel.gametools.common.random.RandomNumber;

/**
 * Class to emulate the rolling of multiple game dice.
 * 
 * @author Rufus
 */
public class GameDice implements Dice {
  // Define instance variables
  private List<Integer> diceList;

  private int numberOfDice;

  private int numberOfSides;

  // Set the minimum number of dice and sides permitted
  private static final int MIN_NUM_DICE = 1;

  private static final int MIN_NUM_SIDES = 3;

  private static final String ERROR_MSG_NUM_SIDES = "A die must have at least " + MIN_NUM_SIDES
      + " sides.";

  private static final String ERROR_MSG_NUM_DICE = "You must have at least " + MIN_NUM_DICE
      + " die.";

  /**
   * Create one die with numSides number of sides.
   * 
   * @param numSides - The number of sides the die should have.
   * @throws IndexOutOfBoundsException if there aren't enough sides.
   */
  public GameDice(final int numSides) throws IndexOutOfBoundsException {
    if (numSides >= MIN_NUM_SIDES) {
      this.numberOfDice = MIN_NUM_DICE;
      this.numberOfSides = numSides;
    } else {
      throw new IndexOutOfBoundsException(ERROR_MSG_NUM_SIDES);
    }
  }

  /**
   * Create numDice dice with numSides number of sides.
   * 
   * @param numDice - The number of GameDice to create.
   * @param numSides - The number of sides each GameDice should have.
   * @throws IndexOutOfBoundsException if there are not enough sides or dice.
   */
  public GameDice(final int numDice, final int numSides) throws IndexOutOfBoundsException {
    if (numDice >= MIN_NUM_DICE) {
      this.numberOfDice = numDice;
      if (numSides >= MIN_NUM_SIDES) {
        this.numberOfSides = numSides;
      } else {
        throw new IndexOutOfBoundsException(ERROR_MSG_NUM_SIDES);
      }
    } else {
      throw new IndexOutOfBoundsException(ERROR_MSG_NUM_DICE);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see net.techbabel.gametools.common.dice.Dice#roll()
   */
  @Override
  public Integer roll() {
    int total = 0;

    this.diceList = new ArrayList<Integer>();

    // Roll each die and place it in the list
    for (int die = 0; die < this.numberOfDice; die++) {
      final int currentDie = RandomNumber.generate(this.numberOfSides);

      this.diceList.add(currentDie);
      total += currentDie;
    }

    return total;
  }

  /*
   * (non-Javadoc)
   * 
   * @see net.techbabel.gametools.common.dice.Dice#getDiceList()
   */
  @Override
  public List<Integer> getDiceList() {
    final List<Integer> theList = new ArrayList<Integer>();
    theList.addAll(this.diceList);

    return theList;
  }
}
