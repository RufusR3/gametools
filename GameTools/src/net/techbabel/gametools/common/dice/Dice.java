package net.techbabel.gametools.common.dice;

import java.util.List;

/**
 * Define the interface that should be utilized.
 * 
 * @author Rufus
 */
public interface Dice {
  /**
   * Roll the dice and return the result.
   * 
   * @return The sum of the dice being rolled.
   */
  Integer roll();

  /**
   * Retrieve a list of the last dice rolled.
   * 
   * @return A list of the last dice rolled.
   */
  List<Integer> getDiceList();
}
